package types;

public interface IType {

	static final IType BOOLEAN = new BoolType();
	static final IType INT = new IntType();

	boolean equals(IType type2);
	

	/**
	 * Get the type name to be used by the compiler
	 * 
	 * @return type name to be used by the compiler
	 */
	String toString();

}
