package types;

public class RefType implements IType {

	private IType subType;

	public RefType(IType type) {
		this.subType = type;
	}

	@Override
	public boolean equals(IType type2) {
		return type2 instanceof RefType;
	}

	@Override
	public String toString() {
		if (subType instanceof IntType || subType instanceof BoolType)
			return "ref_int";
		else
			return "ref_class";
	}

	public IType getSubType() {
		return subType;
	}
}
