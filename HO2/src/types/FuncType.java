package types;

import java.util.Iterator;
import java.util.List;

public class FuncType implements IType {

	private List<IType> paramsTypeList;
	private IType outputType;

	public FuncType(List<IType> types, IType type) {
		this.paramsTypeList = types;
		this.outputType = type;
	}

	@Override
	public boolean equals(IType type2) {
		if (type2 instanceof FuncType) {
			if (((FuncType) type2).getOutputType().equals(outputType)) {
				Iterator<IType> itTypes = ((FuncType) type2).getParamsTypeList().iterator();
				for (IType t : paramsTypeList) {
					IType t2 = itTypes.next();
					if (!t2.equals(t))
						return false;
				}
				return true;
			}
		}
		return false;
	}

	public List<IType> getParamsTypeList() {
		return paramsTypeList;
	}

	public IType getOutputType() {
		return outputType;
	}

}
