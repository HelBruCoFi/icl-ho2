package types;

public class BoolType implements IType {

	@Override
	public boolean equals(IType type2) {
		return type2 instanceof BoolType;
	}

	@Override
	public String toString() {
		return "I";
	}
	
}
