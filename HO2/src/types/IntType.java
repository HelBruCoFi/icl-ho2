package types;

public class IntType implements IType {

	@Override
	public boolean equals(IType type2) {
		return (type2 instanceof IntType);
	}

	@Override
	public String toString() {
		return "I";
	}
	
}
