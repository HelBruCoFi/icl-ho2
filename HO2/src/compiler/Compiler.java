package compiler;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nodes.ASTNode;

public class Compiler {

	private static final String header = ".class public Main\r\n" + ".super java/lang/Object\r\n" + "\r\n" + ";\r\n"
			+ "; standard initializer\r\n" + ".method public <init>()V\r\n" + "   aload_0\r\n"
			+ "   invokenonvirtual java/lang/Object/<init>()V\r\n" + "   return\r\n" + ".end method\r\n" + "\r\n"
			+ ".method public static main([Ljava/lang/String;)V\r\n" + "       ; set limits used by this method\r\n"
			+ "       .limit locals 10 \r\n" + "       .limit stack 256\r\n" + "\r\n"
			+ "       ; setup local variables:\r\n" + "\r\n"
			+ "       ;    1 - the PrintStream object held in java.lang.System.out\r\n"
			+ "       getstatic java/lang/System/out Ljava/io/PrintStream;\r\n" + "\r\n"
			+ "       ; place your bytecodes here\r\n" + "       ; START\r\n" + "\n";
	private static final String footer = "\n" + "       ; END\r\n" + "\r\n" + "\r\n" + "       ; convert to String;\r\n"
			+ "       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;\r\n" + "       ; call println \r\n"
			+ "       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V\r\n" + "\r\n" + "       return\r\n"
			+ "\r\n" + ".end method\r\n" + "";

	private ASTNode exp;

	public Compiler(ASTNode exp) {
		this.exp = exp;
	}

	public void compile(CompileEnvironment env) {
		CodeBlock codeBlock = new CodeBlock();
		exp.compile(env, codeBlock);
		compileFrame(codeBlock);
		compileClosures(codeBlock);

		String fileName = "Main.j";

		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			fos.write(header.getBytes());
			fos.write(codeBlock.toString().getBytes());
			fos.write(footer.getBytes());
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void compileClosures(CodeBlock codeBlock) {
		List<Entry<String, List<String>>> closures = codeBlock.getCodeBlockClosures();
		
		for(Entry<String, List<String>> closure: closures) {
			String fileName = closure.getKey() + ".j";
			List<String> code = closure.getValue();
			try {
				File file = new File(fileName);
				file.createNewFile();
				FileOutputStream fos = new FileOutputStream(file);
				String codes = "";
				for (String str : code)
					codes += str + "\n";
				fos.write(codes.getBytes());
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}

	private void compileFrame(CodeBlock codeBlock) {
		List<List<String>> code = codeBlock.getCodeBlockFrame();
		String fileName = "";
		for (int i = 0; i < code.size(); i++) {
			String codes = "";
			fileName = "f" + i + ".j";

			try {
				File file = new File(fileName);
				file.createNewFile();
				FileOutputStream fos = new FileOutputStream(file);

				for (String str : code.get(i))
					codes += str + "\n";
				fos.write(codes.getBytes());
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
