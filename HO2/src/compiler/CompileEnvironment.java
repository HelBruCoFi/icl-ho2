package compiler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import environment.Address;
import nodes.ASTId;
import types.IType;

public class CompileEnvironment {

	private Map<ASTId, Integer> vars; // Pair Id-Offset
	private Map<ASTId, IType> varTypes; // Pair Id-Type
	private int varCounter;
	private CompileEnvironment parent;

	public CompileEnvironment() {
		vars = new HashMap<ASTId, Integer>();
		varTypes = new HashMap<ASTId, IType>();
		varCounter = 0;
		this.parent = null;
	}

	public CompileEnvironment(CompileEnvironment parent) {
		vars = new HashMap<ASTId, Integer>();
		varTypes = new HashMap<ASTId, IType>();
		varCounter = 0;
		this.parent = parent;
	}

	public Address find(ASTId id) {
		Address res;
		if (vars.containsKey(id)) {
			int offset = vars.get(id);
			IType t = varTypes.get(id);
			res = new Address(0, offset, t);
		} else {
			res = parent.find(id).incLevel();
		}

		return res;
	}
	
	

	public void assoc(ASTId id, IType type) {
		vars.put(id, varCounter++);
		varTypes.put(id, type);
	}

	public CompileEnvironment beginScope(CodeBlock code, int varsSize, List<IType> types) {
		code.addFrame(varsSize, types);
		return new CompileEnvironment(this);
	}
	
	public CompileEnvironment beginScope() {
		return new CompileEnvironment(this);
	}
	

	public CompileEnvironment endScope(CodeBlock code) {
		code.eraseFrame();
		return parent;
	}
	
	public CompileEnvironment endScope() {
		return parent;
	}

}
