package compiler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;

import environment.*;
import types.*;


public class CodeBlock {

	private List<String> codeBlock;
	private List<List<String>> codeBlockFrames;
	private List<Entry<String, List<String>>> codeBlockClosures; 	//<filename, instructions>
	private int frameCounter; // Holds the number of the last frame
	private int closureCounter; // Holds the number of the last closure
	private int gotoLines;
	private List<String> stack;

	public CodeBlock() {
		codeBlock = new LinkedList<String>();
		stack = new LinkedList<String>();
		codeBlockFrames = new LinkedList<List<String>>();
		codeBlockClosures = new LinkedList<Entry<String, List<String>>>();
		frameCounter = -1;
		closureCounter = -1;
		// initialize frame pointer SL stored in local #4 to null
		add("aconst_null");
		add("astore 4");
		gotoLines = 0;
	}

	public void add(String code) {
		codeBlock.add(code);
	}
	
	public void clearCode() {
		codeBlock = new LinkedList<String>();
	}

	public String toString() {
		String codes = "";
		for (String str : codeBlock)
			codes += "	" + str + "\n";
		return codes;
	}

	public List<List<String>> getCodeBlockFrame() {
		return codeBlockFrames;
	}
	
	public List<Entry<String, List<String>>> getCodeBlockClosures() {
		return codeBlockClosures;
	}

	public void addVarVal(int offset, IType type) {
		String varName = "x" + offset;
		String currFrame = "f" + frameCounter;
		if (type instanceof IntType || type instanceof BoolType)
			add("putfield " + currFrame + "/" + varName + " I");
		else if(type instanceof FuncType)
			add("putfield " + currFrame + "/" + varName + " Lclosure_"+closureCounter+";");
		else
			add("putfield " + currFrame + "/" + varName + " Ljava/lang/Object;");
	}

	public void fetchVar(Address varAddress) {
		String currFrame = "";
		String prevFrame = "";
		String varName = "x" + varAddress.getOffset();
		add("aload 4");
		if (stack.size() > 1 && varAddress.getLevel() > 0) {
			currFrame = stack.get(stack.size()-1);
			prevFrame = stack.get((stack.size()-1)-1);
			for (int i = 0; i < varAddress.getLevel(); i++) {
				currFrame = stack.get(stack.size()-1-i);
				prevFrame = stack.get((stack.size()-1)-i-1);
				add("getfield " + currFrame + "/sl L" + prevFrame + ";");
			}
			// Fetch variable
		} else {
			prevFrame = stack.get(stack.size()-1);
		}
		
		if (varAddress.getType() instanceof IntType || varAddress.getType() instanceof BoolType)
			add("getfield " + prevFrame + "/" + varName + " I");
		else if(varAddress.getType() instanceof FuncType)
			add("getfield " + prevFrame + "/" + varName + " Lclosure_"+closureCounter+";");
		else
			add("getfield " + prevFrame + "/" + varName + " Ljava/lang/Object;");

	}

	public void addFrame(int varsSize, List<IType> types) {
		frameCounter++;
		String frameName = "f" + frameCounter;
		stack.add(frameName);
		createNewFrameFile(varsSize, frameName, types);
		// create frame for this let block
		add("new " + frameName);
		add("dup");
		add("invokespecial " + frameName + "/<init>()V");
		add("dup");
		// store SL in new frame
		add("aload 4");
		if (frameCounter == 0) {
			add("putfield " + frameName + "/sl Ljava/lang/Object;"); // Define SL as an Object
		} else {

			add("putfield " + frameName + "/sl Lf" + (frameCounter - 1) + ";"); // Define SL as the previous frame type
		}
		// Update SL
		add("astore 4");

	}

	public void eraseFrame() {
		String frameName = stack.get(stack.size()-1);
		add("aload 4"); // load SL
		// Get previous frame
		if (frameName.equals("f0")) {
			add("getfield " + frameName + "/sl Ljava/lang/Object;");
		} else {

			add("getfield " + frameName + "/sl L" + stack.get(stack.size()-2) + ";");
		}
		// Store it as SL
		add("astore 4");
		stack.remove(frameName);
	}

	/**
	 * Adds the needed instructions to create a frame file
	 * 
	 * @param varsSize - the amount of variable in the frame
	 * @param frameName - The frame name
	 * @param types - A list with the type of each variable
	 */
	private void createNewFrameFile(int varsSize, String frameName, List<IType> types) {
		List<String> frame = new LinkedList<String>();
		int nextClosure = closureCounter+1;

		frame.add(".class " + frameName);
		frame.add(".super java/lang/Object");
		if (frameCounter == 0)
			frame.add(".field public sl Ljava/lang/Object;");
		else
			frame.add(".field public sl Lf" + (frameCounter - 1) + ";");
		for (int i = 0; i < varsSize; i++) {
			IType type = types.get(i);
			if (type instanceof IntType || type instanceof BoolType)
				frame.add((".field public x" + i + " " + types.get(i).toString()));
			else if (type instanceof FuncType)
				frame.add((".field public x" + i + " Lclosure_"+nextClosure+++";"));
			else
				frame.add((".field public x" + i + " Ljava/lang/Object;"));
		}
			
			
		frame.add("");
		frame.add(".method public <init>()V");
		frame.add("aload_0");
		frame.add("invokenonvirtual java/lang/Object/<init>()V");
		frame.add("return");
		frame.add(".end method");

		codeBlockFrames.add(frame);
	}

	public int getGotoLine() {
		return gotoLines++;
	}
	
	public String createNewClosure(List<IType> types, IType retType) {
		List<String> closure = new LinkedList<String>();
		closureCounter++;
		String closureName = "closure_" + closureCounter;
		
		String callType = "(";
		for(IType type: types)
			callType += type.toString();
		callType += ')' + retType.toString();
		
		String typeName = createClosureType(closureName, callType);
		String frameName = createClosureFrame(types, closureName);
		String currFrame = stack.get(stack.size()-1);
		
		
		add("new " + closureName);
		add("dup");
		add("invokespecial "+closureName+"/<init>()V");
		add("dup");
		add("aload 4");
		add("putfield "+closureName+"/sl L"+currFrame+";");

		stack.add(frameName);
		
		closure.add(".class " + closureName);
		closure.add(".super java/lang/Object");
		closure.add(".implements " + typeName);
		closure.add(".field public sl L"+currFrame+";");
		
		closure.add("");
		closure.add(".method public <init>()V");
		closure.add("aload_0");
		closure.add("invokenonvirtual java/lang/Object/<init>()V");
		closure.add("return");
		closure.add(".end method");
		
		
		closure.add("\n" + ".method public call" + callType);
		closure.add(".limit locals 10\n" + 	".limit stack 256");
		
		/*Compilar Fun��o*/
		// create and initalize closure parameters frame
		closure.add("new " + frameName);
		closure.add("dup");
		closure.add("invokespecial " + frameName + "/<init>()V");
		closure.add("dup");
		closure.add("aload 0"); // objecto atual
		closure.add("getfield " + closureName + "/sl L"+currFrame+";");
		closure.add("putfield " + frameName + "/sl L"+currFrame+";");
		// initialize local SL
		closure.add("astore 4");
		// fetch parameters and store then in frame
		for(int i = 0; i < types.size(); i++) {
			closure.add("aload 4");
			closure.add("iload " + (i+1));
			closure.add("putfield "+frameName+"/x"+i+" " + types.get(i).toString());
		}
		
		// Compilar
		
		
		codeBlockClosures.add(new SimpleEntry<String, List<String>>(closureName,closure));
		return closureName;
	}
	
	public void endClosureDef(String name, CodeBlock code) {
		List<String> closure = null;
		for(Entry<String, List<String>> entry: codeBlockClosures)
			if (entry.getKey().equals(name))
				closure = entry.getValue();
		for(String str: code.codeBlock)
			closure.add(str);
		closure.add("ireturn");
		closure.add(".end method");
		String frameName = "f"+name;
		stack.remove(frameName);
	}

	private String createClosureFrame(List<IType> types, String closureName) {
		List<String> closureFrame = new LinkedList<String>();
		String frameName = "f" + closureName;
		
		closureFrame.add(".class " + frameName);
		closureFrame.add(".super java/lang/Object");
		closureFrame.add(".field public sl Lf" + frameCounter + ";");
		for (int i = 0; i < types.size(); i++) {
			IType type = types.get(i);
			if (type instanceof IntType || type instanceof BoolType)
				closureFrame.add((".field public x" + i + " " + types.get(i).toString()));
			else
				closureFrame.add((".field public x" + i + " Ljava/lang/Object;"));
		}
		
		closureFrame.add(".method public <init>()V\n" + 
				"  aload_0\n" + 
				"  invokenonvirtual java/lang/Object/<init>()V\n" + 
				"  return\n" + 
				".end method");
		
		codeBlockClosures.add(new SimpleEntry<String, List<String>>(frameName, closureFrame));
		
		return frameName;
	}

	private String createClosureType(String closureName, String callType) {
		String typeName = closureName + "_type";
		
		List<String> closureType = new LinkedList<String>();
		closureType.add(".interface " + typeName);
		closureType.add(".super java/lang/Object");
		closureType.add(".method public abstract call" + callType);
		closureType.add("return\n" + ".end method");
		
		codeBlockClosures.add(new SimpleEntry<String, List<String>>(typeName, closureType));
		
		return typeName;
	}
	
	public String getLastClosure() {
		return "closure_"+closureCounter;
	}
	
	public CodeBlock clone() {
		CodeBlock clone = new CodeBlock();
		clone.closureCounter = closureCounter;
		clone.codeBlock = new LinkedList<String>(codeBlock);
		clone.stack = new LinkedList<String>(stack);
		clone.codeBlockFrames = new LinkedList<List<String>>(codeBlockFrames);
		clone.codeBlockClosures = new LinkedList<Entry<String, List<String>>>(codeBlockClosures);
		return clone;
	}
}
