package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTAssign implements ASTNode {

	private ASTNode id;
	private ASTNode node;

	public ASTAssign(ASTNode id, ASTNode node) {
		this.id = id;
		this.node = node;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {

		if (id instanceof ASTId) {
			IValue val = env.find(id);
			if (val instanceof VRef) {
				((VRef) val).set(node.eval(env));
			} else if (val == null) {
				val = node.eval(env);
				env.assoc(id, val);
			}
			return val;
		}
		IValue ref = id.eval(env);
		if (ref instanceof VRef)
			((VRef) ref).set(node.eval(env));
		return new VRef(ref);

	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		id.compile(env, code);
		if (id instanceof ASTId) {
			IType type = env.find((ASTId) id).getType();
			String className = "";
			String obj = "";
			if (type instanceof RefType)
				if (((RefType) type).getSubType() instanceof RefType) {
					className = "ref_class";
					obj = "Ljava/lang/Object;";
					code.add("checkcast " + className);
				} else {
					className = "ref_int";
					obj = "I";
					code.add("checkcast " + className);
				}
			node.compile(env, code);
			code.add("putfield " + className + "/v " + obj);
		} else
			node.compile(env, code);

	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		if (id instanceof ASTId) {
			IType type = node.typecheck(env);
			IType t2 = env.find(id);
			if (t2 == null) {
				type = new RefType(type);
				env.assoc(id, type);
				return type;

			} else if (t2 instanceof RefType && ((RefType) t2).getSubType().equals(type))
				return t2;
			throw new TypingException("Illegal type in := operator");
		}
		IType ref = id.typecheck(env);
		if (ref instanceof RefType)
			return new RefType(ref);
		throw new TypingException("Illegal type in := operator");
	}

}
