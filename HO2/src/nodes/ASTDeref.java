package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTDeref implements ASTNode {

	private ASTNode node;
	private IType type;

	public ASTDeref(ASTNode node) {
		this.node = node;
		this.type = null;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue vnode = node.eval(env);
		if (vnode instanceof VRef) {
			return ((VRef) vnode).get();
		}
		throw new TypeErrorException("illegal arguments to ! operator");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		code.add("\t; Compile Deref");
		node.compile(env, code);
		String className = type.toString();
		String obj = "";
		if (((RefType)type).getSubType() instanceof RefType || type instanceof FuncType)
			obj = "Ljava/lang/Object;";
		else
			obj = "I";
		code.add("checkcast " + className);
		code.add("getfield " + className + "/v " + obj);
		code.add("\t; Fim Compile Deref");
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType type = node.typecheck(env);
		if (type instanceof RefType) {
			this.type = type;			
			return ((RefType) type).getSubType();
		}
		throw new TypingException("Illegal type of ! operator");
	}

}
