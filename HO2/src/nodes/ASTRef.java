package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTRef implements ASTNode {

	private ASTNode node;
	private IType type;

	public ASTRef(ASTNode value) {
		this.node = value;
		this.type = null;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue val = node.eval(env);
		return new VRef(val);
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		code.add("\t; Compile Ref");
		String typeName = type.toString();
		code.add("new " + typeName);
		code.add("dup");
		code.add("invokespecial " + typeName + "/<init>()V");
		code.add("dup");
		node.compile(env, code);
		code.add("putfield " + typeName + "/v " + ((RefType) type).getSubType().toString());
		code.add("\t; Fim Compile Ref");
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType t = node.typecheck(env);
		if (!(t instanceof FuncType)) {
			type = new RefType(t);
			return type;
		}
		throw new TypingException("Illegal type in new operator");
	}

}
