package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTBool implements ASTNode {

	private IValue bool;

	public ASTBool(IValue bool) {
		this.bool = bool;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		if (bool instanceof VBool)
			return bool;
		throw new TypeErrorException("not a boolean");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		if (((VBool) bool).getValue())
			code.add("sipush 1");
		else
			code.add("sipush 0");
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		return new BoolType();
	}

}
