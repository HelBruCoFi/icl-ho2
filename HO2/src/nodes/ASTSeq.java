package nodes;

import java.util.List;

import compiler.*;
import environment.*;
import exceptions.*;
import types.IType;
import values.IValue;

public class ASTSeq implements ASTNode {

	private List<ASTNode> body;

	public ASTSeq(List<ASTNode> body) {
		this.body = body;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue val = null;
		for(ASTNode node: body)
			val = node.eval(env);
		return val;
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		for(ASTNode node: body)
			node.compile(env, code);
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType type = null;
		for(ASTNode node: body)
			type = node.typecheck(env);
		return type;
	}

}
