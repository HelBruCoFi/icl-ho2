package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTIfThenElse implements ASTNode {

	private ASTNode cond, ifTrue, ifFalse;

	public ASTIfThenElse(ASTNode cond, ASTNode ifTrue, ASTNode ifFalse) {
		this.cond = cond;
		this.ifTrue = ifTrue;
		this.ifFalse = ifFalse;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue vcond = cond.eval(env);
		if (vcond instanceof VBool) {
			if (((VBool) vcond).getValue())
				return ifTrue.eval(env);
			else
				return ifFalse.eval(env);
		}
		throw new TypeErrorException("illegal arguments to if then else operation");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		cond.compile(env, code);
		String l1 = "L" + code.getGotoLine();
		code.add("ifeq " + l1);
		ifTrue.compile(env, code);
		String l2 = "L" + code.getGotoLine();
		code.add("goto " + l2);
		code.add(l1 + ":");
		ifFalse.compile(env, code);
		code.add(l2 + ":");

	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType condType = cond.typecheck(env);
		IType ifTrueType = ifTrue.typecheck(env);
		IType ifFalseType = ifFalse.typecheck(env);
		
		if (condType instanceof RefType)
			condType = ((RefType) condType).getSubType();
		
		if (condType.equals(IType.BOOLEAN) /*&& ifTrueType.equals(ifFalseType)*/)
			return ifTrueType;
		throw new TypingException("Illegal type on if then else operator");
	}

}
