package nodes;

import java.util.AbstractMap.SimpleEntry;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTLet implements ASTNode {

	private List<SimpleEntry<ASTId, ASTNode>> varsIdNode;
	private ASTNode body;
	private List<IType> types;

	public ASTLet(List<IType> types, List<SimpleEntry<String, ASTNode>> varsIdNode, ASTNode body) {
		this.body = body;
		this.types = types;
		this.varsIdNode = new LinkedList<SimpleEntry<ASTId, ASTNode>>();

		for (Entry<String, ASTNode> var : varsIdNode) {
			SimpleEntry<ASTId, ASTNode> entry = new SimpleEntry<ASTId, ASTNode>(new ASTId(var.getKey()),
					var.getValue());
			this.varsIdNode.add(entry);
		}

	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		ASTId id;
		IValue v1;
		env = env.beginScope();
		IType type;
		Iterator<IType> itTypes = types.iterator();
		for (Entry<ASTId, ASTNode> var : varsIdNode) {
			type = itTypes.next(); // assume-se que h� tantos tipos como vari�veis
			id = var.getKey();
			v1 = var.getValue().eval(env);
			v1 = buildMatchingValue(type, v1);
			env.assoc(id, v1); // id = f, v1 = VFunc
		}
		IValue v2 = body.eval(env);
		env.endScope();
		return v2;
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		// Generate new frame file and call the constructor
		env = env.beginScope(code, varsIdNode.size(), types);
		// Store the variables
		int i = 0;
		for (Entry<ASTId, ASTNode> var : varsIdNode) {
			IType t = types.get(i);
			env.assoc(var.getKey(), t);
			code.add("aload 4");
			var.getValue().compile(env, code);
			code.addVarVal(i, t);
			i++;
		}
		body.compile(env, code);
		env.endScope(code);
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		ASTId id;
		IType t1;
		env = env.beginScope();
		IType type;
		Iterator<IType> itTypes = types.iterator();
		for (Entry<ASTId, ASTNode> var : varsIdNode) {
			type = itTypes.next(); // assume-se que h� tantos tipos como vari�veis
			id = var.getKey();
			t1 = var.getValue().typecheck(env);
			IType matchedType = buildMatchingTypes(t1, type);
			if (t1.equals(matchedType)) {
				env.assoc(id, t1); // id = f, t1 = FuncType
				id.setType(t1);
			}
		}
		IType bodyType = body.typecheck(env);
		env.endScope();
		return bodyType;
	}

	private IValue buildMatchingValue(IType type, IValue val) throws TypeErrorException {
		if (type instanceof IntType && val instanceof VInt)
			return val;
		else if (type instanceof BoolType && val instanceof VBool)
			return val;
		else if (type instanceof RefType) {
			if (val instanceof VRef)
				return val;
			else
				return new VRef(buildMatchingValue(((RefType) type).getSubType(), val));
		} else if (type instanceof FuncType && val instanceof VFunc)
			return val;

		throw new TypeErrorException("Type missmatch on let operator");
	}

	/**
	 * @param varType           - is always a basic type
	 * @param typeFromTypesList - can be a reference for a basic type, or another
	 *                          reference
	 */
	private IType buildMatchingTypes(IType varType, IType typeFromTypesList) throws TypingException {
		if (varType.equals(typeFromTypesList))
			return varType;
		else if (typeFromTypesList instanceof RefType)
			return buildMatchingTypes(varType, ((RefType) typeFromTypesList).getSubType());

		throw new TypingException("Invalid type on let operator");

	}

}
