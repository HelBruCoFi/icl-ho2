package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTAnd implements ASTNode {

	private ASTNode left, right;

	public ASTAnd(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue vl = left.eval(env);
		if (vl instanceof VBool) {
			IValue vr = right.eval(env);
			if (vr instanceof VBool) {
				if ((((VBool) vl).getValue() == ((VBool) vr).getValue()) == true)
					return new VBool(true);
				else
					return new VBool(false);
			}
		}
		throw new TypeErrorException("illegal arguments to && operator");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		left.compile(env, code);
		right.compile(env, code);
		code.add("iand");

	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType leftType = left.typecheck(env);
		IType rightType = right.typecheck(env);
			
		if (leftType.equals(IType.BOOLEAN) && rightType.equals(IType.BOOLEAN))
			return IType.BOOLEAN;
		throw new TypingException("Illegal type on op &&");
	}

}
