package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.IValue;

public interface ASTNode {

	IValue eval(Environment<IValue> env) throws TypeErrorException;

	void compile(CompileEnvironment env, CodeBlock code);

	IType typecheck(Environment<IType> env) throws TypingException;
}
