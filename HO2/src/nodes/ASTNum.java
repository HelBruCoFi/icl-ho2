package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTNum implements ASTNode {

	private IValue val;

	public ASTNum(IValue _val) {
		val = _val;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		if (val instanceof VInt)
			return val;
		throw new TypeErrorException("not a number");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		code.add("sipush " + ((VInt) val).getValue());
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		return new IntType();
	}

	public IValue getValue() {
		return val;
	}
}
