package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTId implements ASTNode {

	private String id;
	private IType type;

	public ASTId(String id) {
		this.id = id;
		type = null;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		return env.find(this);
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		code.fetchVar(env.find(this));
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType t = env.find(this);
		if (t != null) {
			type = t;
			return t;
		}
		throw new TypingException("Type missmatch in id: " + id);
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof ASTId)
			return id.equals(object.toString());
		return false;
	}

	public void setType(IType t) {
		type = t;
	}

	public IType getType() {
		return type;
	}
}
