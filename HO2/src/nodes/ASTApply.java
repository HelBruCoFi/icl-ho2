package nodes;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTApply implements ASTNode {

	private ASTId funcId;
	private VFunc function;
	private List<ASTNode> argsList;
	private List<IValue> valuesList;

	public ASTApply(ASTId astId, List<ASTNode> valuesList) {
		funcId = astId;
		this.argsList = valuesList;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue func = env.find(funcId);
		if (func instanceof VFunc) {
			function = (VFunc) func;
			valuesList = new LinkedList<IValue>();
			for (ASTNode arg : argsList) {
				IValue val = null;
				if (arg instanceof ASTNum)
					val = ((ASTNum) arg).getValue();
				else if (arg instanceof ASTId)
					val = env.find((ASTId) arg);
				valuesList.add(val);
			}
			return function.calcResult(valuesList, env);
		}
		throw new TypeErrorException("not a function in apply");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		String closureType = code.getLastClosure() + "_type";
		FuncType t = (FuncType) env.find(funcId).getType();
		funcId.compile(env, code);
		for(ASTNode node: argsList) {
			node.compile(env, code);
		}
		String callType = "(";
		for (IType type: t.getParamsTypeList())
			callType += type.toString();
		callType += ")" + t.getOutputType().toString();
		code.add("invokeinterface "+closureType+"/call"+callType+" "+(t.getParamsTypeList().size()+1));
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType type = env.find(funcId);
		if (type instanceof FuncType) {
			FuncType funcType = (FuncType) type;
			IValue argVal;
			IType argType;
			Iterator<IValue> itVals = valuesList.iterator();
			Iterator<IType> itTypes = funcType.getParamsTypeList().iterator();
			while (itVals.hasNext() && itTypes.hasNext()) {
				argVal = itVals.next();
				argType = itTypes.next();

				if (argVal instanceof VInt && argType instanceof IntType
						|| argVal instanceof VBool && argType instanceof BoolType
						|| argVal instanceof VRef && argType instanceof RefType
						|| argVal instanceof VFunc && argType instanceof FuncType)
					; // do nothing, keep going
				else
					throw new TypingException("applying wrong arguments types in function");

				if (!itVals.hasNext() && itTypes.hasNext() || itVals.hasNext() && !itTypes.hasNext())
					throw new TypingException("applying wrong number of arguments in function");

			}
			return funcType.getOutputType(); // not sure
		}
		throw new TypingException("not a function ID in apply");

	}

}
