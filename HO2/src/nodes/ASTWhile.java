package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTWhile implements ASTNode {

	private ASTNode cond, body;

	public ASTWhile(ASTNode cond, ASTNode body) {
		this.cond = cond;
		this.body = body;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue vcond = cond.eval(env);
		if (vcond instanceof VBool) {

			while (((VBool) cond.eval(env)).getValue()) {
				body.eval(env);
			}
			return null;
		}
		throw new TypeErrorException("illegal arguments to while operation");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		String l1 = "L" + code.getGotoLine();
		code.add(l1 + ":");
		cond.compile(env, code);
		String l2 = "L" + code.getGotoLine();
		code.add("ifeq " + l2);
		body.compile(env, code);
		code.add("goto " + l1);
		code.add(l2 + ":");
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType condType = cond.typecheck(env);
		if (!(condType instanceof BoolType))
			throw new TypingException("Illegal type of ! operator");

		IType bodyType = body.typecheck(env);
		return bodyType;
	}

}
