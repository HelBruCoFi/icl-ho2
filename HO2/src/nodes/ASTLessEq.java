package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTLessEq implements ASTNode {

	private ASTNode left, right;

	public ASTLessEq(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue vl = left.eval(env);
		if (vl instanceof VInt) {
			IValue vr = right.eval(env);
			if (vr instanceof VInt) {
				return new VBool(((VInt) vl).getValue() <= ((VInt) vr).getValue());
			}
		}
		throw new TypeErrorException("illegal arguments to <= operator");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		left.compile(env, code);
		right.compile(env, code);
		int l1 = code.getGotoLine();
		code.add("if_icmple L" + l1);
		code.add("sipush 0");
		int l2 = code.getGotoLine();
		code.add("goto L" + l2);
		code.add(String.format("L%d:", l1));
		code.add("sipush 1");
		code.add(String.format("L%d: ", l2));
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType leftType = left.typecheck(env);
		IType rightType = right.typecheck(env);
		
		if (leftType.equals(IType.INT) && rightType.equals(IType.INT))
			return IType.BOOLEAN;
		throw new TypingException("Illegal type on op <=");
	}

}
