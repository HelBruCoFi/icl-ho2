package nodes;

import compiler.*;
import environment.Environment;
import exceptions.*;
import types.IType;
import values.*;

public class ASTSub implements ASTNode {

	private ASTNode left, right;

	public ASTSub(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue vl = left.eval(env);
		if (vl instanceof VInt) {
			IValue vr = right.eval(env);
			if (vr instanceof VInt) {
				return new VInt(((VInt) vl).getValue() - ((VInt) vr).getValue());
			}
		}
		throw new TypeErrorException("illegal arguments to - operator");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		left.compile(env, code);
		right.compile(env, code);
		code.add("isub");
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType leftType = left.typecheck(env);
		IType rightType = right.typecheck(env);

		if (leftType.equals(IType.INT) && rightType.equals(IType.INT))
			return IType.INT;
		throw new TypingException("Illegal type on op -");
	}

}
