package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTNeg implements ASTNode {

	private ASTNode node;

	public ASTNeg(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue vnode = node.eval(env);
		if (vnode instanceof VBool) {
			return new VBool(!((VBool) vnode).getValue());
		}
		throw new TypeErrorException("illegal arguments to ~ operator");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		node.compile(env, code);
		String l1 = "L" + code.getGotoLine();
		code.add("ifeq " + l1);
		code.add("sipush 0");
		String l2 = "L" + code.getGotoLine();
		code.add("goto " + l2);
		code.add(l1 + ":");
		code.add("sipush 1");
		code.add(l2 + ":");
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType nodeType = node.typecheck(env);
		if (nodeType.equals(IType.BOOLEAN))
			return IType.BOOLEAN;
		throw new TypingException("Illegal type on boolean operator");
	}

}
