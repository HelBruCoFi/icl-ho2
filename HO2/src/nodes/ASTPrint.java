package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTPrint implements ASTNode {

	private ASTNode node;

	public ASTPrint(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue val = node.eval(env);
		System.out.println(val);
		return val;
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		code.add("\t; Compile Print");
		code.add("getstatic java/lang/System/out Ljava/io/PrintStream; ");
		node.compile(env, code);
		code.add("dup\r\n" + 
				"	istore_2\r\n" + 
				"	invokevirtual java/io/PrintStream/println(I)V\r\n" + 
				"	\r\n" + 
				"	iload_2");
		code.add("\t; Fim Compile Print");
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		return node.typecheck(env);
	}

}
