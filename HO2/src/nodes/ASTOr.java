package nodes;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTOr implements ASTNode {

	private ASTNode left, right;

	public ASTOr(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		IValue vl = left.eval(env);
		if (vl instanceof VBool) {
			IValue vr = right.eval(env);
			if (vr instanceof VBool) {
				if (((VBool) vl).getValue() || ((VBool) vr).getValue())
					return new VBool(true);
				else
					return new VBool(false);
			}
		}
		throw new TypeErrorException("illegal arguments to || operator");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		left.compile(env, code);
		right.compile(env, code);
		code.add("ior");
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		IType typeLeft = left.typecheck(env);
		IType typeRight = right.typecheck(env);
		
		if (typeLeft.equals(IType.BOOLEAN) && typeRight.equals(IType.BOOLEAN))
			return IType.BOOLEAN;
		throw new TypingException("Illegal type on op ||");
	}

}
