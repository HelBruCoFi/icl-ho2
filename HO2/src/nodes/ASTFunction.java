package nodes;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import compiler.*;
import environment.*;
import exceptions.*;
import types.*;
import values.*;

public class ASTFunction implements ASTNode {

	private List<ASTId> listParamIdType;
	private IValue body;
	private FuncType funcType;

	public ASTFunction(List<ASTId> listParamIdType, IValue body) {
		this.listParamIdType = listParamIdType;
		this.body = body;
		this.funcType = null;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws TypeErrorException {
		if (body instanceof VFunc) { // guarda os IDs no environment
			for (ASTId id : listParamIdType)
				env.assoc(id, null);
			((VFunc) body).setIds(listParamIdType);
			return body;
		}
		throw new TypeErrorException("not a function");
	}

	@Override
	public void compile(CompileEnvironment env, CodeBlock code) {
		String closureName = code.createNewClosure(funcType.getParamsTypeList(), funcType.getOutputType());
		env = env.beginScope();
		CodeBlock temp = code.clone();
		temp.clearCode();
		for(int i = 0; i < listParamIdType.size(); i++)
			env.assoc(listParamIdType.get(i), funcType.getParamsTypeList().get(i));
		((VFunc)body).getBody().compile(env, temp);
		env = env.endScope();
		code.endClosureDef(closureName, temp);
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypingException {
		ASTId id;
		IType type;
		Iterator<ASTId> itIds = listParamIdType.iterator();
		List<IType> paramsTypes = new LinkedList<IType>();

		while (itIds.hasNext()) {
			id = itIds.next();
			type = id.getType();
			paramsTypes.add(type);
			env.assoc(id, type);
		}

		IType bodyType = ((VFunc) body).getBody().typecheck(env);
		funcType = new FuncType(paramsTypes, bodyType);
		return funcType;
	}

}
