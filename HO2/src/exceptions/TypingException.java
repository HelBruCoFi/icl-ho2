package exceptions;

public class TypingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TypingException() {
		super();
	}

	/** Constructor with message. */
	public TypingException(String message) {
		super(message);
	}

}
