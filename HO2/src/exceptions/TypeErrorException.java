package exceptions;

public class TypeErrorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TypeErrorException() {
		super();
	}

	/** Constructor with message. */
	public TypeErrorException(String message) {
		super(message);
	}
}
