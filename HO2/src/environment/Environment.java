package environment;

import java.util.HashMap;
import java.util.Map;
import nodes.*;

public class Environment<T> {

	private Environment<T> parent;
	private Map<ASTNode, T> vars;

	public Environment() {
		parent = null;
		vars = new HashMap<ASTNode, T>();
	}

	public Environment(Environment<T> parent) {
		this.parent = parent;
		vars = new HashMap<ASTNode, T>();
	}

	public Environment<T> beginScope() {
		return new Environment<T>(this);
	}

	public void assoc(ASTNode id, T value) {
		if (id instanceof ASTId || id instanceof ASTRef)
			vars.put(id, value);
		else
			System.err.println("Error in Environment association, key is not an ASTId nor ASTRef");
	}

	public T find(ASTNode id) {
		if (id instanceof ASTId || id instanceof ASTRef) {
			T res = vars.get(id);
			if (res == null) {
				if (parent != null)
					res = parent.find(id);
				else
					System.err.println("ID not valid");
			}
			return res;
		}
		System.err.println("ID not valid");
		return null;
	}

	public Environment<T> endScope() {
		return parent;
	}
}
