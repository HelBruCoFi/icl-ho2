package environment;

import types.IType;

public class Address {

	private int level, offset;
	private IType type;

	public Address(int level, int offset, IType t) {
		this.level = level;
		this.offset = offset;
		this.type = t;
	}

	public int getLevel() {
		return level;
	}

	public int getOffset() {
		return offset;
	}

	public Address incLevel() {
		level++;
		return this;
	}
	
	public IType getType() {
		return type;
	}
}
