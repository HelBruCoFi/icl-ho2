package values;

import java.util.Iterator;
import java.util.List;

import environment.Environment;
import exceptions.*;
import nodes.*;
import types.*;

public class VFunc implements IValue {

	private List<ASTId> paramsIds;
	private List<IValue> args;
	private IValue output;
	private ASTNode body;

	public VFunc(ASTNode body) {
		this.body = body;
	}

	public void setIds(List<ASTId> paramsIds) {
		this.paramsIds = paramsIds;
	}

	public IValue calcResult(List<IValue> args, Environment<IValue> env) throws TypeErrorException {
		ASTId astid;
		IValue value;
		Iterator<ASTId> itIds = paramsIds.iterator();
		Iterator<IValue> itValues = args.iterator();
		while (itIds.hasNext() && itValues.hasNext()) {
			astid = itIds.next();
			value = itValues.next();
			value = buildMatchingValue(astid.getType(), value);
			env.assoc(astid, value);

			if (!itIds.hasNext() && itValues.hasNext() || itIds.hasNext() && !itValues.hasNext())
				throw new TypeErrorException("different number of IDs and types in VFunc");
		}

		output = body.eval(env);
		return output;
	}

	public ASTNode getBody() {
		return body;
	}

	private IValue buildMatchingValue(IType type, IValue val) throws TypeErrorException {
		if (type instanceof IntType && val instanceof VInt)
			return val;
		else if (type instanceof BoolType && val instanceof VBool)
			return val;
		else if (type instanceof RefType) {
			if (val instanceof VRef)
				return val;
			else
				return new VRef(buildMatchingValue(((RefType) type).getSubType(), val));
		} else if (type instanceof FuncType && val instanceof VFunc)
			return val;

		throw new TypeErrorException("Type missmatch on calcResult in VFunc");
	}
}
