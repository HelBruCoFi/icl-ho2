package values;

public class VRef implements IValue {

	private IValue value;

	public VRef(IValue value) {
		this.value = value;
	}

	public void set(IValue value) {
		this.value = value;
	}

	public IValue get() {
		return value;
	}

	public String toString() {
		return value.toString();
	}
}
