package values;

public class VInt implements IValue {

	private int value;

	public VInt(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public String toString() {
		Integer val = value;
		return val.toString();
	}

}
