package values;

public class VBool implements IValue {

	private boolean bool;

	public VBool(boolean bool) {
		this.bool = bool;
	}

	public boolean getValue() {
		return bool;
	}

	public String toString() {
		Boolean bol = bool;
		return bol.toString();
	}
}
